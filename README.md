# MintBox

This is a [cdist](https://www.cdi.st/) [type](https://www.cdi.st/manual/latest/cdist-type.html)
to quickly set up my workspace on a graphical workstation.

It takes inspirations from my ["debian reset"](https://gitlab.com/romain-dartigues/cdist/__p0d_debian_reset)
without sticking to it, mainly because this type embed some workarounds and other specificities for
[LinuxMint](https://linuxmint.com/) and will not work well other distributions.


## Install

```bash
# requirements
pip install --user cdist
mkdir -p ~/.cdist/type
cd ~/.cdist/type

# choose one of:
git clone git@gitlab.com:romain-dartigues/cdist/__p0d_mintbox.git ||
git clone https://gitlab.com/romain-dartigues/cdist/__p0d_mintbox.git

# or:
wget -O- https://gitlab.com/romain-dartigues/cdist/__p0d_mintbox/-/archive/master/__p0d_mintbox-master.tar.gz |
tar -xvz &&
mv -v __p0d_mintbox-master __p0d_mintbox
```

## Usage

Basic usage:

```bash
cdist config -vv -i ~/.cdist/type/__p0d_mintbox/manifest localhost [-n]
```

## Resources

- https://www.cdi.st/manual/latest/cdist-reference.html
- https://www.cdi.st/manual/latest/cdist-type.html
- https://www.cdi.st/manual/latest/cdist-explorer.html
